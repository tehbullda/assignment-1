#pragma once

template <class T>
class LinkedList {
public:
	template <class T>
	struct Node {
		T data;
		Node *next;
	};
	LinkedList();
	~LinkedList();

	void push_back(T data);
	void push_front(T data);

	void pop_back();
	void pop_front();

	bool search(T data);
	int size();

	void erase(const int &index);

	void clear();

private:
	Node<T> *m_root;
};

template <class T>
LinkedList<T>::LinkedList() {
	m_root = nullptr;
}

template <class T>
LinkedList<T>::~LinkedList() {
	if (m_root) {
		clear();
	}
}

template <class T>
void LinkedList<T>::push_back(T data) {
	Node<T> *newNode = new Node<T>{ data, nullptr };
	if (m_root == nullptr) {
		m_root = newNode;
		return;
	}
	if (m_root->next == nullptr) {
		m_root->next = newNode;
	}
	else {
		Node<T> *current = m_root->next;
		while (current->next != nullptr) {
			current = current->next;
		}
		current->next = newNode;
	}
};

template <class T>
void LinkedList<T>::push_front(T data) {
	Node<T> *newNode = new Node<T>;
	newNode->data = data;
	newNode->next = m_root;
	m_root = newNode;
};

template <class T>
void LinkedList<T>::pop_back() {
	if (m_root->next == nullptr){
		delete m_root;
		m_root = nullptr;
	}
	if (m_root != nullptr) {
		Node<T> *current = m_root, *previous = nullptr;
		while (current->next != nullptr) {
			previous = current;
			current = current->next;
		}
		delete current;
		current = nullptr;
		previous->next = nullptr;
	}
}

template <class T>
void LinkedList<T>::pop_front() {
	if (m_root != nullptr) {
		if (m_root->next != nullptr) {
			Node<T> *tmp = m_root->next;
			delete m_root;
			m_root = tmp;
		}
		else {
			delete m_root;
			m_root = nullptr;
		}
	}
}

template <class T>
bool LinkedList<T>::search(T data) {
	if (m_root != nullptr) {
		if (m_root->data == data) {
			return true;
		}
		Node<T> *current = m_root;
		while (current->next != nullptr) {
			if (current->next->data == data) {
				return true;
			}
			current = current->next;
		}
	}
	return false;
}

template <class T>
int LinkedList<T>::size() {
	int size = 0;
	if (m_root == nullptr) {
		return size;
	}
	Node<T> *current = m_root;
	++size; //The root exists, count it
	while (current->next != nullptr) {
		++size;
		current = current->next;
	}
	return size;
}

template <class T>
void LinkedList<T>::erase(const int &index) {
	int curr = 0;
	if (m_root == nullptr || index > size()-1) {
		return;
	}
	Node<T> *current = m_root, *previous = m_root;
	while (curr != index) {
		++curr;
		previous = current;
		current = current->next;
	}
	previous->next = current->next;
	delete current;
}

template <class T>
void LinkedList<T>::clear() {
	while (m_root) {
		pop_back();
	}
}