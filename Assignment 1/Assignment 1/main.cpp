// Assignment 1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "LinkedList.h"
#include "BST.h"
#include <iostream>
#include <vld.h>

void LinkedListTests() {
	std::cout << "LinkedListTests" << std::endl;
	LinkedList<int> linkedlist;
	linkedlist.push_back(5);
	if (linkedlist.search(5)) {
		std::cout << "Found 5" << std::endl;
	}
	linkedlist.push_front(10);
	linkedlist.push_back(4);
	linkedlist.push_front(7);
	linkedlist.push_back(10); // Testing duplicate
	std::cout << "Size: " << linkedlist.size() << std::endl;
	if (linkedlist.search(4)) {
		std::cout << "Found 4" << std::endl;
	}
	linkedlist.pop_back();
	std::cout << "Size: " << linkedlist.size() << std::endl;
	linkedlist.pop_front();
	if (linkedlist.search(4)) {
		std::cout << "Found 4" << std::endl;
	}
	std::cout << "Size: " << linkedlist.size() << std::endl;
	linkedlist.clear();
	std::cout << "Size after clear: " << linkedlist.size() << std::endl;
}

void BSTTests() {
	std::cout << "BSTTests" << std::endl;
	BST<int> BST;
	BST.insert(5);
	if (BST.search(5)) {
		std::cout << "Found 5, size: " << BST.size() << std::endl;
	}
	BST.insert(8);
	if (BST.search(8)) {
		std::cout << "Found 8, size: " << BST.size() << std::endl;
	}
	BST.insert(3);
	if (BST.search(3)) {
		std::cout << "Found 3, size: " << BST.size() << std::endl;
	}
	BST.insert(2);
	if (BST.search(2)) {
		std::cout << "Found 2, size: " << BST.size() << std::endl;
	}
	BST.insert(4);
	BST.insert(4);
	BST.insert(6);
	BST.insert(105);
	BST.insert(55);
	BST.insert(80);
	BST.insert(60);
	BST.insert(61);
	BST.insert(11);
	BST.insert(75);
	BST.insert(75);
	BST.insert(74);
	BST.insert(102);
	BST.insert(34);
	if (BST.search(102)) {
		std::cout << "Found 102, size: " << BST.size() << std::endl;
	}
	BST.erase(5);
	BST.erase(105);
	BST.erase(60);
	BST.erase(55);
	BST.erase(75);
	BST.erase(60);
	BST.erase(5);
	BST.erase(322);
	std::cout << "BST in order: ";
	BST.traverse_in_order();
	std::cout << std::endl << "BST pre order: ";
	BST.traverse_pre_order();
	std::cout << std::endl << "BST post order: ";
	BST.traverse_post_order();
	BST.clear();
	std::cout << std::endl << "Size after clear: " << BST.size() << std::endl;
}

int _tmain(int argc, _TCHAR* argv[])
{
	(void)argc;
	(void)argv;
	
	LinkedListTests();
	BSTTests();

	std::cin.get();
	return 0;
}

