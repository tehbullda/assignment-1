#pragma once

template <class T>
class BST
{
public:
	template <typename U>
	struct Node {
		Node(U data, Node *left, Node *right) { m_data = data; m_left = left; m_right = right; };
		U m_data;
		Node *m_left;
		Node *m_right;
	};
	BST();
	~BST();

	void insert(const T &data);
	void erase(const T &data);
	bool search(const T &data);

	void traverse_in_order();
	void traverse_pre_order();
	void traverse_post_order();

	int size();

	void clear();

private:
	void insert(Node<T> *node, const T &data);
	void erase(Node<T> *curr, Node<T> *prev, const T &data);
	bool search(Node<T> *curr, const T &data);

	void traverse_in_order(Node<T> *curr);
	void traverse_pre_order(Node<T> *curr);
	void traverse_post_order(Node<T> *curr);

	T find_lowest_value(Node<T> *curr);
	T find_highest_value(Node<T> *curr);

	int size(Node<T> *node);

private:
	Node<T> *m_root;
};

template <class T>
BST<T>::BST(){
	m_root = nullptr;

};

template <class T>
BST<T>::~BST(){

};

template <class T>
void BST<T>::insert(const T &data) {
	if (!m_root) {
		m_root = new Node<T>(data, nullptr, nullptr);
		return;
	}
	else if (m_root->m_data == data) {
		return;
	}
	else {
		insert(m_root, data);
	}
}

template <class T>
void BST<T>::insert(Node<T> *node, const T &data) {
	if (node->m_data == data) {
		return;
	}
	else if (node->m_data > data) {
		if (node->m_left) {
			insert(node->m_left, data);
		}
		else {
			node->m_left = new Node<T>(data, nullptr, nullptr);
		}
	}
	else {
		if (node->m_right) {
			insert(node->m_right, data);
		}
		else {
			node->m_right = new Node<T>(data, nullptr, nullptr);
		}
	}
}

template <class T>
void BST<T>::erase(const T &data) {
	if (!m_root) {
		return;
	}
	erase(m_root, nullptr, data);
}

template <class T>
void BST<T>::erase(Node<T> *curr, Node<T> *prev, const T &data) {
	if (curr) {
		if (curr->m_data == data) {
			if (!curr->m_left && !curr->m_right) { // If no children
				if (prev) {
					if (prev->m_left == curr) {
						prev->m_left = nullptr;
					}
					else if (prev->m_right == curr) {
						prev->m_right = nullptr;
					}
				}
				else { // If root
					m_root = nullptr;
				}
				delete curr;
			}
			else if (!curr->m_left && curr->m_right) { // If 1 child (right)
				if (prev) {
					if (prev->m_left == curr) {
						prev->m_left = curr->m_right;
					}
					else if (prev->m_right == curr) {
						prev->m_right = curr->m_right;
					}
				}
				else {
					m_root = curr->m_right;
				}
				delete curr;
			}
			else if (curr->m_left && !curr->m_right) { // If 1 child (left)
				if (prev) {
					if (prev->m_left == curr) {
						prev->m_left = curr->m_left;
					}
					else if (prev->m_right == curr) {
						prev->m_right = curr->m_left;
					}
				}
				else {
					m_root = curr->m_left;
				}
				delete curr;
			}
			else { // If 2 children
				if (size(curr->m_left) > size(curr->m_right)) { //Find highest in left tree
					T tmp = find_highest_value(curr->m_left);
					erase(curr->m_left, curr, tmp);
					curr->m_data = tmp;
				}
				else { //Find lowest in right tree
					T tmp = find_lowest_value(curr->m_right);
					erase(curr->m_right, curr, tmp);
					curr->m_data = tmp;
				}
			}
		}
		else {
			if (curr->m_data > data) {
				erase(curr->m_left, curr, data);
			}
			else {
				erase(curr->m_right, curr, data);
			}
		}
	}
}

template <class T>
T BST<T>::find_lowest_value(Node<T> *curr) {
	if (curr->m_left) {
		return find_lowest_value(curr->m_left);
	}
	return curr->m_data;
}

template <class T>
T BST<T>::find_highest_value(Node<T> *curr) {
	if (curr->m_right) {
		return find_highest_value(curr->m_right);
	}
	return curr->m_data;
}

template <class T>
bool BST<T>::search(const T &data) {
	if (m_root) {
		return search(m_root, data);
	}
	else {
		return false;
	}
}

template <class T>
bool BST<T>::search(Node<T> *node, const T &data) {
	if (node->m_data == data) {
		return true;
	}
	else if (node->m_data > data) {
		if (node->m_left) {
			return search(node->m_left, data);
		}
		else {
			return false;
		}
	}
	else {
		if (node->m_right) {
			return search(node->m_right, data);
		}
		else {
			return false;
		}
	}
}

template <class T>
int BST<T>::size() {
	return size(m_root);
}

template <class T>
int BST<T>::size(Node<T> *node) {
	return !node ? 0 : size(node->m_left) + size(node->m_right) + 1;
}

template <class T>
void BST<T>::clear() {
	while (m_root) {
		erase(m_root->m_data);
	}
	m_root = nullptr;
}

template <class T>
void BST<T>::traverse_in_order() {
	if (m_root) {
		traverse_in_order(m_root);
	}
}

template <class T>
void BST<T>::traverse_in_order(Node<T> *curr) {
	if (curr) {
		traverse_in_order(curr->m_left);
		printf("%d ", curr->m_data);
		traverse_in_order(curr->m_right);
	}
}

template <class T>
void BST<T>::traverse_pre_order() {
	if (m_root) {
		traverse_pre_order(m_root);
	}
}
template <class T>
void BST<T>::traverse_pre_order(Node<T> *curr) {
	printf("%d ", curr->m_data);

	if (curr->m_left) {
		traverse_pre_order(curr->m_left);
	}
	if (curr->m_right)
		traverse_pre_order(curr->m_right);
}

template <class T>
void BST<T>::traverse_post_order() {
	if (m_root) {
		traverse_post_order(m_root);
	}
}

template <class T>
void BST<T>::traverse_post_order(Node<T> *curr) {
	if (curr->m_left) {
		traverse_post_order(curr->m_left);
	}
	if (curr->m_right) {
		traverse_post_order(curr->m_right);
	}
	printf("%d ", curr->m_data);
}